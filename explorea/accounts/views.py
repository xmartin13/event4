from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, get_user_model, update_session_auth_hash, login
from django.contrib.auth.forms import PasswordChangeForm

from .forms import RegisterForm, EditProfileForm

def change_password(request):

    if not request.user.is_authenticated:
        return redirect('login')

    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)

        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('/accounts/profile/')
        else:
            return render(request, 'accounts/change_password.html', {'form': form})

    form = PasswordChangeForm(user=request.user)
    return render(request, 'accounts/change_password.html', {'form': form})

def edit_profile(request):

    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)

        if form.is_valid():
            user = form.save()
            return redirect('profile')

    form = EditProfileForm(instance=request.user)
    return render(request, 'accounts/edit_profile.html', {'form': form} )

def profile(request):

    #return render(request, 'accounts/profile.html', {'user': request.user})
    return render(request, 'accounts/profile.html')


def register(request):

    if request.method == 'POST':

        form = RegisterForm(request.POST)

        if form.is_valid():
            user = form.save()

            raw_password = form.cleaned_data.get('password1')

            user = authenticate(username=user.username, password=raw_password)
            login(request, user)

            return redirect('profile')
        else:
            return render(request, 'accounts/register.html', {'form': form} )

    form = RegisterForm()
    return render(request, 'accounts/register.html', {'form': form} )

"""
def register(request):

    if request.method == 'POST':

        # create the form and populate it with data from the POST request
        form = RegisterForm(request.POST)

        # validate the data in the form
        if form.is_valid():

            # get the validated data from the form
            username = form.cleaned_data.get('username')
            email = form.cleaned_data.get('email')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            password = form.cleaned_data.get('password1')

            # create and save the user
            User = get_user_model()
            u = User(username=username,
                     email=email,
                     first_name=first_name,
                     last_name=last_name)
            u.set_password(password)
            u.save()

            user = authenticate(username=username, password=password)
            login(request, user)

            # redirect to another page
            return redirect('profile')

    # if the request is of type GET
    form = RegisterForm()
    return render(request, 'accounts/register.html', {'form': form} )
"""
