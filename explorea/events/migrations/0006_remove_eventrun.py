from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_auto_20181116_1446'),
    ]

    operations = [
        migrations.DeleteModel(
           name='eventrun',
        ),
    ]
