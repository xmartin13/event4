from django.urls import path

from . import views

app_name = 'events'

urlpatterns = [
    path('', views.index, name='index'),
    path('events/', views.event_listing, name='events'),
    path('eventrun/', views.eventrun_listing, name='eventrun'),
    #path('event/<str:name>', views.event_detail, name='detail'),
    path('events/detail/<int:pkey>', views.event_detail, name='detail_event'),
    path('events/update/<int:pkey>', views.update_event, name='update_event'),
    path('events/delete/<int:pkey>', views.delete_event, name='delete_event'),
    path('eventrun/detail/<int:pkey>', views.eventrun_detail, name='detail_eventrun'),
    path('eventrun/update/<int:event_run_id>', views.update_event_run, name='update_event_run'),
    path('eventrun/delete/<int:event_run_id>', views.delete_event_run, name='delete_event_run'),
    path('events/new/', views.create_event, name='create_event'),
    path('events/mine/', views.my_event, name='my_event'),
    path('events/mine/detail/<int:pkey>', views.my_event_detail, name='detail_my_event'),
    path('events/<int:event_id>/newrun/', views.create_event_run, name='create_event_run'),
]
