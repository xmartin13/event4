from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from .models import Event, EventRun
from .forms import EventForm, EventRunForm


def index(request):
    return render(request, 'events/index.html')


def event_listing(request):

    events = Event.objects.all()

    return render(request, 'events/event_listing.html', {'events': events})

def eventrun_listing(request):

    events = EventRun.objects.all()

    return render(request, 'events/eventrun_listing.html', {'events': events})

def event_detail(request, pkey):

    event = Event.objects.get(pk=pkey)
    runs = event.eventrun_set.all().order_by('date', 'time')
    args = {'event': event, 'runs': runs}

    return render(request, 'events/event_detail.html', args)

def my_event_detail(request, pkey):

        event = Event.objects.get(pk=pkey)
        runs = event.eventrun_set.all().order_by('date', 'time')
        args = {'event': event, 'runs': runs}

        return render(request, 'events/my_event_detail.html', args)

def eventrun_detail(request, pkey):

    eventrun = EventRun.objects.get(pk=pkey)

    return render(request, 'events/eventrun_detail.html', {'run': eventrun})

@login_required
def create_event(request):

    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            event = form.save(commit=False)
            event.host = request.user
            event.save()

            return redirect('my_event')

    form = EventForm()
    return render(request, 'events/create_event.html', {'form': form})

@login_required
def my_event(request):

    events = Event.objects.filter(host_id=request.user.id)

    return render(request, 'events/my_event.html', {'events': events})

def create_event_run(request, event_id):

    if request.method == 'POST':
        form = EventRunForm(request.POST)

        if form.is_valid():
            event_run = form.save(commit=False)
            event_run.event = Event.objects.get(pk=event_id)
            event_run.save()

            url = '/events/mine/detail/{}'.format(event_id)
            return redirect(url)

    args = {'form': EventRunForm()}
    return render(request, 'events/create_event_run.html', args)

def delete_event(request, pkey):

    Event.objects.get(pk=pkey).delete()
    return redirect('my_event')

def delete_event_run(request, event_run_id):

    run = EventRun.objects.get(pk=event_run_id)
    event_id = run.event.id
    run.delete()

    url = '/events/mine/detail/{}'.format(event_id)
    return redirect(url)

def update_event(request, pkey):
    event = Event.objects.get(pk=pkey)

    if request.method == 'POST':
        form = EventForm(request.POST, instance=event)

        if form.is_valid():
            event = form.save()
            return redirect('my_event')

    form = EventForm(instance=event)
    return render(request, 'events/create_event.html', {'form': form})

def update_event_run(request, event_run_id):

    event_run = EventRun.objects.get(pk=event_run_id)

    if request.method == 'POST':
        form = EventRunForm(request.POST, instance=event_run)

        if form.is_valid():
            event_run = form.save()
            event_id = event_run.event.id
            url = '/eventrun/detail/{}'.format(event_run_id)
            return redirect(url)

    args = {'form': EventRunForm(instance=event_run)}
    return render(request, 'events/update_eventrun.html', args)

"""
def event_detail(request, name):
    data = {'Chill' : '<h2>Chill on the beach just for $400</h2>',
            'Camping': '<h2>Camp with us for $50</h2>',
            'Flying': '<h2>Fly for free</h2>'}

    selection = data.get(name)

    if selection:
        return HttpResponse(selection)
    else:
        return HttpResponse('No such event in offering for now')
"""
